<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language ?>" xml:lang="<?php print $language ?>">
<head>
<title><?php print $head_title ?></title>
<meta http-equiv="Content-Style-Type" content="text/css" />
    <?php print $head ?>
    <?php print $styles ?>
    <?php print $scripts ?>
</head>
<body <?php print theme("onload_attribute"); ?>>
<div id="wrap">
  <div id="header"><div id="headerimg">
      <h1><a href="<?php print url() ?>" title="<?php print($site_name) ?>"><?php print($site_name) ?></a></h1>
      <div class="slogan"><?php print($site_slogan) ?></div>
    </div>
    <?php if (isset($primary_links)) : ?>
        <div id="navigation">
      		<?php print theme('links', $primary_links, array('class' => 'links primary-links')) ?>
    	</div>
    <?php endif; ?>
  </div>
  <div id="middle">
  <div id="left_col" class="narrowcolumn">
    <h2 class="topmost"><?php print ($site_name) ?></h2>
    <div class="navigation"> <?php print $breadcrumb ?> </div>
    <?php if ($messages != ""): ?>
    <div id="message"><?php print $messages ?></div>
    <?php endif; ?>
    <?php if ($mission != ""): ?>
    <div id="mission"><?php print $mission ?></div>
    <?php endif; ?>
    <?php if ($title != ""): ?>
    <h2 class="page-title"><?php print $title ?></h2>
    <?php endif; ?>
    <?php if ($tabs != ""): ?>
    <?php print $tabs ?>
    <?php endif; ?>
    <?php if ($help != ""): ?>
    <p id="help"><?php print $help ?></p>
    <?php endif; ?>
    <!-- start main content -->
    <?php print($content) ?>
    <!-- end main content -->
  </div>
  <div id="right_col">
    <?php if ($search_box): ?>
		<?php print $search_box ?>
    <?php endif; ?>
    <?php print $left ?> <?php print $right ?> </div>
  <br />
  <div id="footer">
    <p>
      <?php if ($footer_message) : ?>
      <?php print $footer_message;?><br />
      <?php endif; ?>
      <a href="/node/feed/">RSS</a> <strong>|</strong> <a href="http://www.roobottom.com/wp_theme/">Design by Jon J Roobottom</a>, ported to Drupal by <a href="http://goodbasic.com/">GoodBasic</a>.</p>
  </div>
</div></div>
<?php print $closure;?>
</body>
</html>