<div class="comments <?php print ($comment->new) ? 'comment-new' : '' ?>"> 
  <?php if ($comment->new) : ?> 
  <a id="new"></a> <span class="new"><?php print $new ?></span> 
  <?php endif; ?> 
  <div class="content">
  <?php print $picture ?> <cite>On <?php print $date ?>, <?php print $author ?> said:</cite><br />  
  <?php print $content ?></div> 
  <?php if ($picture) : ?> 
  <br class="clear" /> 
  <?php endif; ?> 
  <p class="postmetadata"><?php print $links ?> &#187;</p> 
</div>